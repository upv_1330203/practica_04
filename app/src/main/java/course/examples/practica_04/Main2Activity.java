package course.examples.practica_04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        // Crea la vista para el texto
        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText(message);
        //Configurar la vista del texto como el layout de la actividad
        setContentView(textView);
    }

}
